clc
clear all
close all

load("gradients.mat")
load("projections.mat")
load("zg_spectrum.mat")

if ~exist("art3dx.oct", "file")
  mkoctfile("art3dx.cc")
endif

if ~exist("pr3dx.oct", "file")
  mkoctfile("pr3dx.cc")
endif

n = size(prj0, 1);
nn = size(spc, 1);
k3 = size(xyz, 1);

m = 128;
fov = 2.5;
sw = 24;

xyz = xyz * fov / m * n / sw;

bpr = zeros(m, m, m);
n_itr = 12;

tic
for itr = 1 : n_itr
      
  [~, shuffle] = sort(rand(k3, 1));
  bpr = art3dx(bpr, prj0(:, shuffle), xyz(shuffle,: ), spc);
  bpr(bpr < 0.0) = 0.0;
  
  preview3d(bpr)
  drawnow

  tl = round(toc);
  printf("Iteration #%d\tElapsed time = %02d:%02d\n", ...
         itr, floor(tl / 60), mod(tl, 60))
endfor

prjs = zeros(n, k3);
prjs = pr3dx(bpr, prjs, xyz, spc);

figure
set(gcf, "position", [680 80 1080 880])

for j = 1 : 4
  rnd = ceil(rand * k3);
  
  subplot(2, 2, j)
  plot(prj0(:, rnd))
  hold on
  plot(prjs(:, rnd))
  hold off

  str = sprintf("Projection #%d", rnd);
  title(str)
endfor

drawnow

res = prj0(:) - prjs(:);
tot = prj0(:) - mean(prj0(:));
R2 = 1 - (res' * res) / (tot' * tot);

printf("Reconstruction is done. Final R2 = %.3f\n", R2)

return
##
